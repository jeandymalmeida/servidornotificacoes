/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package http;

import static http.GravarArquivo.ler_arquivo;
import static http.FabricarPropriedades.COD_SERVIDOR;
import static http.FabricarPropriedades.CRIAR_GRUPOS;
import static http.FabricarPropriedades.ENVIA_NOTIFICACAO;
import static http.FabricarPropriedades.JSON;
import static http.FabricarPropriedades.SERVIDOR_KEY;
import java.io.DataInputStream;
import java.io.DataOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Jeanderson
 */
public class ServidorNotificacao extends Thread {

    /*-------------------------MENSAGEM DE ERROS E SUCESSOS ---------------*/
    public static String MENSAGEM_FAILURE = " DADOS RECEBIDOS NO FCM COM ICONSISTÊNCIA, REVER DADOS ";
    public static String MENSAGEM_SUCESS  = " DADOS RECEBIDOS NO FCM COM SUCESSO !! ";
    public static String RETORNO_SUCESSO  = " NOTIFICAÇÃO KEY GERADA COM SUCESSO !! \n ";
    public static String RETORNO_FALHA    = " FALHA AO GERAR NOTIFICATION KEY !! ";
    public static String RETORNO_ERRO     = " ERRO AO GERAR CHAVE DA NOTIFICACAO ";

    public static void main(String args[]) throws IOException {
        
        //Construindo parametros para arquivo.
        FabricarPropriedades.FabricaPropreidade();
        
        
        //Iniciando Servidor de Notificações.
        ServerSocket servidorNotificacao = null;

        try {

            servidorNotificacao = new ServerSocket(63300);

            while (true) {
                System.out.println("SERVIDOR INICIADO, AGUARDANDO CONEXOES...");

                Socket socket = servidorNotificacao.accept();

                Thread t = new ServidorNotificacao(socket);
                t.start();

            }

        } catch (IOException e) {
            System.out.println("Erro ao iniciar o servidor na porta 63300 \n");
            System.out.println(e.getMessage());
        }

    }
    private Socket s;

    ServidorNotificacao(Socket accept) {
        s = accept;
    }



    @Override
    public void run() {

        try {

            DataInputStream in = new DataInputStream(s.getInputStream());
            DataOutputStream out = new DataOutputStream(s.getOutputStream());

            String metodo = in.readUTF();

            switch (metodo) {

                case "ADICIONAR_AO_GRUPO":

                    out.writeBoolean(true);

                    System.out.println(" METODO RECONHECIDO ");
                    String tokenUsuario = in.readUTF();
                    String bairroUsuario = in.readUTF();
                    
                    tokenUsuario = tokenUsuario.trim();
                    bairroUsuario = bairroUsuario.trim();
                   

                    // String chaveNotificao = in.readUTF();
                    System.out.println(" DADOS RECEBIDOS COM SUCESSO " + " TOKEN_USUARIO " + tokenUsuario);
                    System.out.println(" DADOS RECEBIDOS COM SUCESSO " + " BAIRRO_USUS" + bairroUsuario);
                    

                    //String token_usuario, String notification_key_name, String notification_key
                    String resultado_grupo = adiciona_grupo_notificacao(tokenUsuario, bairroUsuario, GravarArquivo.ler_arquivo(bairroUsuario));
                    System.out.println("ARQUIVO: " + GravarArquivo.ler_arquivo(bairroUsuario));

                    if (resultado_grupo.equals(RETORNO_FALHA)) {
                        System.out.println(resultado_grupo);
                        out.writeBoolean(false);

                    } else {
                        System.out.println(RETORNO_SUCESSO);

                        //GravarArquivo.gravar_arquivo(nomeNotificacao, resultado_grupo, chaveNotificao);
                        out.writeBoolean(true);
                    }
                    in.close();
                    out.close();

                    break;

                case "CRIAR_GRUPO_NOTIFICACAO":

                    //Variavel para capturar resultado
                    String  resultadoCriacaoGrupo;

                    DataInputStream entrada = new DataInputStream(s.getInputStream());
                    DataOutputStream saida = new DataOutputStream(s.getOutputStream());

                    saida.writeBoolean(true);

                    System.out.println(" METODO RECONHECIDO ");

                    String tokenUsuario_ = entrada.readUTF();
                    String nomeGrupo = entrada.readUTF();
                    String nomeBairroUsuario = entrada.readUTF();

                    System.out.println(" DADOS RECEBIDOS COM SUCESSO " + " TOKEN_USUARIO " + tokenUsuario_);
                    System.out.println(" DADOS RECEBIDOS COM SUCESSO " + " BAIRRO_USUS   " + nomeGrupo);

                    // VERIFICANDO SE O BAIRRO ENVIADO PELO APP JÁ POSSUI UMA NOTIFICAÇÃO KEY
                    String resultadoLeit = ler_arquivo(nomeBairroUsuario);

                    if (resultadoLeit.equals("NÃO ENCONTRADO")) {

                        resultadoCriacaoGrupo =  criar_grupo_de_notificoes(tokenUsuario_, nomeGrupo);
                        
                        if (resultadoCriacaoGrupo.equals(RETORNO_FALHA)) {

                            System.out.println(RETORNO_FALHA);
                            saida.writeBoolean(false);

                        } else {

                            System.out.println(RETORNO_SUCESSO);
                            GravarArquivo.gravar_arquivo(nomeGrupo, resultadoCriacaoGrupo, nomeBairroUsuario);
                            saida.writeBoolean(true);

                        }

                    } else {
                        System.out.println("Bairro já possui uma notificacion key");
                        saida.writeBoolean(true);
                    }

                    break;
                    
                case "ENVIAR_NOTIFICACAO":
                       
                    DataInputStream entradaEnviarNot = new DataInputStream(s.getInputStream());
                    DataOutputStream saidaEnviarNot = new DataOutputStream(s.getOutputStream());

                    //String token_usuario, String titulo, String corpo_mensagem
                    saidaEnviarNot.writeBoolean(true);

                    System.out.println(" METODO RECONHECIDO ");

                    String envTokenUsuario = entradaEnviarNot.readUTF();
                    String envTitulo = entradaEnviarNot.readUTF();
                    String envCorpoMensagem = entradaEnviarNot.readUTF();
                    String envnomeBairroUsuario = entradaEnviarNot.readUTF();

                    System.out.println(" DADOS RECEBIDOS COM SUCESSO " + " TOKEN_USUARIO " + envTokenUsuario);
                    System.out.println(" DADOS RECEBIDOS COM SUCESSO " + " TITULO  " + envTitulo);
                    System.out.println(" DADOS RECEBIDOS COM SUCESSO " + " CORPO_MENSAGEM  " + envCorpoMensagem);
                    System.out.println(" DADOS RECEBIDOS COM SUCESSO " + " BAIRRO_USUARIO  " + envnomeBairroUsuario);

                    // VERIFICANDO SE O BAIRRO ENVIADO PELO APP JÁ POSSUI UMA NOTIFICAÇÃO KEY
                    String envResultadoLeit = ler_arquivo(envnomeBairroUsuario);
                    
                    System.out.println(envResultadoLeit);
                    
                    if (envResultadoLeit.equals("NÃO ENCONTRADO")) {

                        String resultadoCriaGrupo = criar_grupo_de_notificoes(envTokenUsuario, envnomeBairroUsuario);
                        System.out.println(resultadoCriaGrupo.replace(RETORNO_SUCESSO, ""));
                        System.out.println(resultadoCriaGrupo.replace("", RETORNO_SUCESSO));

                        GravarArquivo.gravar_arquivo(envnomeBairroUsuario, resultadoCriaGrupo.replace(RETORNO_SUCESSO, ""), envnomeBairroUsuario);

                        if (!resultadoCriaGrupo.equals(RETORNO_ERRO)) {
                            System.out.println("GRUPO CRIADO COM SUCESSO");
                            String resultadoAdicionaGrupo = adiciona_grupo_notificacao(envTokenUsuario, envnomeBairroUsuario, resultadoCriaGrupo);

                            if (!resultadoAdicionaGrupo.equals(RETORNO_FALHA)) {
                                System.out.println("TOKEN ADICIONADO COM SUCESSO");

                                //GravarArquivo.gravar_arquivo(envnomeBairroUsuario, resultadoCriaGrupo, envnomeBairroUsuario); 
                                saidaEnviarNot.writeBoolean(true);

                            } else {
                                saidaEnviarNot.writeBoolean(false);
                            }
                        } else {
                            saidaEnviarNot.writeBoolean(false);
                        }

                    } else {
                        System.out.println("Bairro já possui uma notificacion key");

                        envia_notificacao(envResultadoLeit, envTitulo, envCorpoMensagem);
                        adiciona_grupo_notificacao(envTokenUsuario, envnomeBairroUsuario, envResultadoLeit);
                        GravarArquivo.gravar_arquivo(envnomeBairroUsuario, envResultadoLeit, envnomeBairroUsuario);

                        saidaEnviarNot.writeBoolean(true);
                    }

                    break;

                default:

                    out.writeUTF(" METODO NÃO RECONHECIDO ");

                    System.out.println(" METODO  NÃO RECONHECIDO ");

                    break;
            }

        } catch (IOException e) {
            System.out.println("Houve falha na conexão do servidor erlang");
            System.out.println(e.getMessage());
        }

    }

    /*----------------- Adiciona Token a um grupo de notificções já criado --------------------------*/
    public static String adiciona_grupo_notificacao(String token_usuario, String notification_key_name, String notification_key) {

        try {

            //Instancia objeto para request
            URL url = new URL(CRIAR_GRUPOS);

            // Instancia o objeto para conexao de request
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);

            //Cabeçalho da requisição 
            con.addRequestProperty("Authorization", SERVIDOR_KEY);
            con.addRequestProperty("Content-Type", JSON);
            con.addRequestProperty("project_id", COD_SERVIDOR);

            JSONObject data = new JSONObject();

            //Http Requisicao
            data.put("operation", "add");
            data.put("notification_key_name", notification_key_name);
            data.put("notification_key", notification_key);
            data.put("registration_ids", new JSONArray(Arrays.asList(token_usuario)));

            //cria canal de saida de dados
            OutputStream os = con.getOutputStream();
            os.write(data.toString().getBytes("UTF-8"));
            os.close();

            // Lê a resposta em  formato de string
            InputStream is = con.getInputStream();
            String responseString = new Scanner(is, "UTF-8").useDelimiter("\\A").next();
            is.close();

            // Converte resposta para o  formato json e pega a notification_Key 
            JSONObject resposta = new JSONObject(responseString);
            System.out.println(resposta.toString());

            String notificatio_key = resposta.getString("notification_key");

            if (!resposta.getString("notification_key").isEmpty()) {
                return RETORNO_SUCESSO + resposta.getString("notification_key");
            } else {
                return RETORNO_FALHA;
            }

        } catch (IOException e) {
            e.printStackTrace();

        }
        return RETORNO_ERRO;
    }

    /*---------------- Criando Grupo de notificações ------------------*/
    public static String criar_grupo_de_notificoes(String token_usuario, String nome_grupo) {

        try {

            //Instancia objeto para request
            URL url = new URL(CRIAR_GRUPOS);

            // Instancia o objeto para conexao de request
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            con.setDoOutput(true);

            //Cabeçalho da requisição 
            con.addRequestProperty("Authorization", SERVIDOR_KEY);
            con.addRequestProperty("Content-Type", JSON);
            con.addRequestProperty("project_id", COD_SERVIDOR);

            // HTTP requisicao
            JSONObject data = new JSONObject();

            data.put("operation", "Create");
            data.put("notification_key_name", nome_grupo);
            data.put("registration_ids", new JSONArray(Arrays.asList(token_usuario)));

            //Verifica os dados de json
            System.out.println(data.toString());

            //cria canal de saida de dados
            OutputStream os = con.getOutputStream();
            os.write(data.toString().getBytes("UTF-8"));
            os.close();

            // Lê a resposta em  formato de string
            InputStream is = con.getInputStream();
            String responseString = new Scanner(is, "UTF-8").useDelimiter("\\A").next();
            is.close();

            // Converte resposta para o  formato json e pega a notification_Key 
            JSONObject resposta = new JSONObject(responseString);
            System.out.println(resposta.toString());

            if (!resposta.getString("notification_key").isEmpty()) {
                return resposta.getString("notification_key");
            } else {
                System.out.println(resposta.getString("failure"));
                return RETORNO_ERRO;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return RETORNO_FALHA;
    }

    /*---------------- Enviando notificações individuais ------------------*/
    public static String envia_notificacao(String token_usuario, String titulo, String corpo_mensagem) {

        try {

            //Instancia objeto para request
            URL url = new URL(ENVIA_NOTIFICACAO);

            // Instancia o objeto para conexao de request
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);

            //Cabeçalho da requisição 
            con.addRequestProperty("Authorization", SERVIDOR_KEY);
            con.addRequestProperty("Content-Type", JSON);
            con.addRequestProperty("project_id", COD_SERVIDOR);

            // HTTP requisicao
            JSONObject data = new JSONObject();
            JSONObject data_1 = new JSONObject();

            data_1.put("title", titulo);
            data_1.put("text", corpo_mensagem);

            data.put("notification", data_1);
            data.put("to", token_usuario);

            System.out.println(data.toString());

            //cria canal de saida de dados
            OutputStream os = con.getOutputStream();
            os.write(data.toString().getBytes("UTF-8"));
            os.close();

            // Lê a resposta em  formato de string
            InputStream is = con.getInputStream();
            String responseString = new Scanner(is, "UTF-8").useDelimiter("\\A").next();
            is.close();

            // Converte resposta para o  formato json e pega a notification_Key 
            JSONObject resposta = new JSONObject(responseString);

            System.out.println(resposta.getInt("failure"));
            System.out.println(resposta.getInt("success"));
            //System.out.println(resposta.getInt("canonical_ids"));
            System.out.println(resposta.toString());

            if (resposta.getInt("failure") == 1) {
                return MENSAGEM_FAILURE;
            } else {

                if (resposta.getInt("success") == 1) {

                    return MENSAGEM_SUCESS;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();

        }

        return MENSAGEM_SUCESS;
    }

}

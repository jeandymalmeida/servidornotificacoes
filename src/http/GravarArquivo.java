
package http;

import static http.FabricarPropriedades.ARQUIVO;

import java.io.BufferedWriter;
import java.lang.Throwable;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jeanderson.almeida   
 */

public class GravarArquivo {

    //VARIAVEIS PARA IMPRESSÃO
    private static String in_bairro;
    private static String in_notification_name;
    private static String in_notification_key;
    
    public static void main(String[] args) throws IOException, IOException, IOException {
        try {
            gravar_arquivo("Teste","testando","Teste");
        } catch (IOException ex) {
            Logger.getLogger(GravarArquivo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //Inicia Metodo para gravar aquivo
    public static void gravar_arquivo(String notification_name, String notification_key, String bairro_usuario) throws IOException {

        //Verifica se o arquivo existe 
        if (ARQUIVO.exists()) {

           //Deixa o arquivo em modo de escrita,e gravacao no final do arquivo (ARQUIVO, true);
            FileWriter arquivo_notificacoes = new FileWriter(ARQUIVO, true);
            BufferedWriter escrever_arquivo = new BufferedWriter(arquivo_notificacoes);
            
            //Cria uma nova linha e escreve a data e hora atual,caminho do arquivo
            escrever_arquivo.newLine();
            escrever_arquivo.write("Arquivo gravado em : " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
            escrever_arquivo.newLine();

            escrever_arquivo.write("Caminho da gravação: " + "src/"+ARQUIVO.getPath());
            escrever_arquivo.newLine();

            escrever_arquivo.write(bairro_usuario + ";" + notification_name + ";" + notification_key);
            escrever_arquivo.newLine();

            //Criando o conteúdo do arquivo
            escrever_arquivo.flush();

            System.out.println("Arquivo gravado em: " + "src/"+ARQUIVO.getPath());
            escrever_arquivo.write(bairro_usuario + ";" + notification_name + ";" + notification_key);
            escrever_arquivo.newLine();
            arquivo_notificacoes.flush();

            //Fechando conexão e escrita do arquivo.
            arquivo_notificacoes.close();

        } else {

            FileWriter arquivo_notificacoes = new FileWriter(ARQUIVO, true);
            BufferedWriter escrever_arquivo = new BufferedWriter(arquivo_notificacoes);

            escrever_arquivo.newLine();
            escrever_arquivo.write("Arquivo gravado em : " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
            escrever_arquivo.newLine();
            escrever_arquivo.write("Caminho da gravação: " + "src/"+ARQUIVO.getPath());
            escrever_arquivo.newLine();
            escrever_arquivo.write(bairro_usuario + ";" + notification_name + ";" + notification_key);
            escrever_arquivo.newLine();

            //Criando o conteúdo do arquivo
            escrever_arquivo.flush();

            //Fechando conexão e escrita do arquivo.
            escrever_arquivo.close();
            System.out.println("Arquivo gravado em: " + "src/"+ARQUIVO.getPath());

        }

    }

    public static String ler_arquivo(String bairro_usuario)  {
        
        try{ 

        FileReader ler_arquivo = new FileReader(ARQUIVO);
        Scanner in = new Scanner(ler_arquivo).useDelimiter("\\;|\\n");

        int i = 1;

        //ABRE O ARQUIVO PARA LEITURA
        if (ler_arquivo.ready()) {

            //Verificando a quantidade de linhas do 
            LineNumberReader linhaLeitura = new LineNumberReader(new FileReader(ARQUIVO));
            linhaLeitura.skip(ARQUIVO.length());
            int qtdLinha = linhaLeitura.getLineNumber();
            System.out.println(qtdLinha);
                  
            //FAZ A LEITURA DE TODO O ARQUIVO 
            while (in.hasNext()) {
                
                System.out.println(in_bairro = in.next());
                System.out.println(in_notification_name = in.next());
                System.out.println(in_notification_key = in.next());

                //PEGA O BAIRRO SOLICITADO
                if (in_bairro.equals(bairro_usuario) && i == 1) {

                    System.out.println("Bairro: " + in_bairro);
                    System.out.println("Notification_name: " + in_notification_name);
                    System.out.println("Notification_key:  " + in_notification_key);
                    i++;
                    ler_arquivo.close();

                    return in_notification_key;

                }
              
            }
            return "NÃO ENCONTRADO";
        }
        return "NÃO ENCONTRADO";
    
        
        
    }catch (Throwable ex) {
            ex.printStackTrace();
    return "NÃO ENCONTRADO";}
    
    }
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package http;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Jeanderson
 */
public class FabricarPropriedades {

    //CRIA, ADICIONA , REMOVE DE GRUPO
    public static String CRIAR_GRUPOS;

    //ENVIA NOTIFICACOES INDIVIDUAIS OU EM GRUPOS
    public static String ENVIA_NOTIFICACAO;

    //ID_TOKEN DO SERVIDOR DE NOTIFICACOES, SERVIDOR_KEY 
    public static String SERVIDOR_KEY;

    //ID_TOKEN_APLICATIVO key do aplicativo 
    public static String APLICATIVO_KEY;

    //ID DO APP PARA CONTATO COM FIREBASE   
    public static String ID_APLICATIVO;

    //CODIGO DO SERVIDOR 
    public static String COD_SERVIDOR;

    //GRUPOS CRIADOS PARA TESTE ,GRUPO TCC 
    public static String CHAVE_NOTIFICACAO;

    //TIPO DO CONTAINER DE RETORNO
    public static String JSON;

    //Caminho do arquivo de notificações
    public static String PASTA;
    
    //CRIANDO O ARQUIVO PARA ARMAZENAMENTO DAS NOTIFICAÇÕES
    public static File ARQUIVO = new File("Notificacoes.txt");
    

    public static Properties getProp() throws IOException {

        File arquivoPropriedades;

        arquivoPropriedades = new File("ProprierdadesDoAplicativo.txt");

        if (arquivoPropriedades.exists()) {

            System.out.println(" ARQUIVO  EXISTE E FOI ENCONTRADO -  " + arquivoPropriedades.getPath());

            Properties props = new Properties();
            FileInputStream file = new FileInputStream(arquivoPropriedades);
            props.load(file);
            return props;

        } else {

            System.out.println(" ARQUIVO NÃO EXISTE ");

            arquivoPropriedades = new File("ProprierdadesDoAplicativo.txt");

            Properties props = new Properties();
            FileInputStream file = new FileInputStream(arquivoPropriedades);
            props.load(file);

            return props;

        }

    }

    public  static void FabricaPropreidade () throws IOException {

        Properties prop = getProp();

        //PEGANDO DADOS DO ARQUIVO DE PROPRIEDADES  
        setCRIAR_GRUPOS(prop.getProperty("prop.app.criar_grupos"));
        setENVIA_NOTIFICACAO(prop.getProperty("prop.app.envia_notificacao"));
        setSERVIDOR_KEY(prop.getProperty("prop.app.servidor_key"));
        setAPLICATIVO_KEY(prop.getProperty("prop.app.aplicativo_key"));
        setID_APLICATIVO(prop.getProperty("prop.app.id_aplicativo"));
        setCOD_SERVIDOR(prop.getProperty("prop.app.cod_servidor"));
        setCHAVE_NOTIFICACAO(prop.getProperty("prop.app.chave_notificacao"));
        setJSON(prop.getProperty("prop.app.json"));
        setPASTA(prop.getProperty("prop.app.pasta"));

        //MOSTRANDO OS DADOS CAPTURADOS DO  ARQUIVO 
        System.out.println("CRIAR_GRUPOS = " + getCRIAR_GRUPOS());
        System.out.println("ENVIA_NOTIFICACAO = " + getENVIA_NOTIFICACAO());
        System.out.println("SERVIDOR_KEY = " + getSERVIDOR_KEY());
        System.out.println("APLICATIVO_KEY = " + getAPLICATIVO_KEY());
        System.out.println("ID_APLICATIVO = " + getID_APLICATIVO());
        System.out.println("COD_SERVIDOR = " + getCOD_SERVIDOR());
        System.out.println("CHAVE_NOTIFICACAO = " + getCHAVE_NOTIFICACAO());
        System.out.println("JSON = " + getJSON());
        System.out.println("PASTA = " + getPASTA());

    }

    public static String getCRIAR_GRUPOS() {
        return CRIAR_GRUPOS;
    }

    public static void setCRIAR_GRUPOS(String CRIAR_GRUPOS) {
        FabricarPropriedades.CRIAR_GRUPOS = CRIAR_GRUPOS;
    }

    public static String getENVIA_NOTIFICACAO() {
        return ENVIA_NOTIFICACAO;
    }

    public static void setENVIA_NOTIFICACAO(String ENVIA_NOTIFICACAO) {
        FabricarPropriedades.ENVIA_NOTIFICACAO = ENVIA_NOTIFICACAO;
    }

    public static String getSERVIDOR_KEY() {
        return SERVIDOR_KEY;
    }

    public static void setSERVIDOR_KEY(String SERVIDOR_KEY) {
        FabricarPropriedades.SERVIDOR_KEY = SERVIDOR_KEY;
    }

    public static String getAPLICATIVO_KEY() {
        return APLICATIVO_KEY;
    }

    public static void setAPLICATIVO_KEY(String APLICATIVO_KEY) {
        FabricarPropriedades.APLICATIVO_KEY = APLICATIVO_KEY;
    }

    public static String getID_APLICATIVO() {
        return ID_APLICATIVO;
    }

    public static void setID_APLICATIVO(String ID_APLICATIVO) {
        FabricarPropriedades.ID_APLICATIVO = ID_APLICATIVO;
    }

    public static String getCOD_SERVIDOR() {
        return COD_SERVIDOR;
    }

    public static void setCOD_SERVIDOR(String COD_SERVIDOR) {
        FabricarPropriedades.COD_SERVIDOR = COD_SERVIDOR;
    }

    public static String getCHAVE_NOTIFICACAO() {
        return CHAVE_NOTIFICACAO;
    }

    public static void setCHAVE_NOTIFICACAO(String CHAVE_NOTIFICACAO) {
        FabricarPropriedades.CHAVE_NOTIFICACAO = CHAVE_NOTIFICACAO;
    }

    public static String getJSON() {
        return JSON;
    }

    public static void setJSON(String JSON) {
        FabricarPropriedades.JSON = JSON;
    }

    public static String getPASTA() {
        return PASTA;
    }

    public static void setPASTA(String PASTA) {
        FabricarPropriedades.PASTA = PASTA;
    }
}
